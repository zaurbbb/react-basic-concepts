import React from "react";

import List from "./components/List";
import { Title } from "./components/Title";

import "./App.css";

function App() {
  const [isDark, setTheme] = React.useState(false);

  function changeTheme () {
    if (isDark) {
      document.body.classList.remove('dark');
    } else {
      document.body.classList.add('dark');
    }
    setTheme(!isDark);
  }
  return (
      <div className="App">
        <label className="switch" htmlFor="checkbox">
          <input type="checkbox" id="checkbox" onClick={ changeTheme  } />
          <div className="slider round"></div>
        </label>
        { Title }
        <List />
    </div>
  );
}

export default App;
