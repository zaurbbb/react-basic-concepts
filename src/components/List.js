import React, { memo } from "react";

const List = () => {
  return (
    <ul className="planets-list">
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>
  );
};

export default memo(List);
